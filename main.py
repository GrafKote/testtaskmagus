import time
import random
from module import proc
from multiprocessing import Pool


# Может быть любой функций
def target_funk(task):
    delay = task["delay"]
    id = task["id"]

    print(f'treadId#{id} Я Уснул на {delay} секунд')
    time.sleep(delay)
    print('Я проснулся!')


def main():
    worker = proc.worker
    results = []
    with Pool(processes=1) as pool:
        for i in range(10):
            task = [{
                "delay": random.randint(0, 4),
                "id": i
            }]
            # Может быть любой функций в параметре func и аргументы для функции в arg
            r = worker.add_task(func=target_funk, arg=task, pool=pool)
            results.append(r)

        # Ждет результаты работы функций в пуле
        worker.await_task(results=results)


if __name__ == '__main__':
    main()