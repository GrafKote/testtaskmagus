class worker:
    def add_task(func, arg, pool):
        print('start')
        p = pool.map_async(func, arg)
        return p

    def await_task(results):
        for out in results:
            out.get()
        results.clear()

